---
wsId: phemexTR
title: 'Phemex TR: TL ile Bitcoin Al'
altTitle: 
authors:
- danny
users: 10000
appId: com.phemex.turkey
appCountry: 
released: 2021-09-30
updated: 2022-09-27
version: 1.2.5
stars: 
ratings: 
reviews: 
size: 
website: https://phemex.com.tr/
repository: 
issue: 
icon: com.phemex.turkey.png
bugbounty: 
meta: stale
verdict: custodial
date: 2023-09-22
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Phemex TR
features: 

---

Related to:

- {% include walletLink.html wallet='android/com.phemex.app' verdict='true' %}
- {% include walletLink.html wallet='iphone/com.phemex.submit' verdict='true' %}

## App Description from Google Play

> With over 5 million users worldwide, Phemex serves as one of the largest cryptocurrency exchanges. You can download the Phemex TR application to buy and sell more than 200 cryptocurrencies in Turkish lira with zero commission opportunities.
>
> Phemex, which has never experienced a case of cyber attacks in its history, is the number one in protecting its users' funds with its **cold wallet system** that defines an independent address for each user!

## Analysis

- The app's listed developer website is issuing an error.
- The app describes its use of *cold wallets*
- This app is **custodial.**
