---
wsId: GreenBitcoinWallet
title: 'Green: Bitcoin Wallet'
altTitle: 
authors:
- leo
users: 100000
appId: com.greenaddress.greenbits_android_wallet
appCountry: 
released: 2015-01-01
updated: 2023-11-23
version: 4.0.18
stars: 4.6
ratings: 946
reviews: 126
size: 
website: https://blockstream.com/green
repository: https://github.com/Blockstream/green_android
issue: https://github.com/Blockstream/green_android/issues/169
icon: com.greenaddress.greenbits_android_wallet.png
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2023-11-26
signer: 32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
reviewArchive:
- date: 2023-04-27
  version: 4.0.1
  appHash: 70199817fafe959a29aa91024f7d9dfd8561489768f32a2d30bc7570c582bf73
  gitRevision: c802fff9e6ed27a7234c463edd5eae8ffe065b4a
  verdict: nonverifiable
- date: 2022-11-01
  version: 3.8.9
  appHash: c318f32b88543ffb89b3f1f6bab589007358c253c03711cc93c98f3b6e30f254
  gitRevision: c8239d1de8b6982590daf5d7222f0f8bc991e185
  verdict: reproducible
- date: 2022-08-07
  version: 3.8.2
  appHash: 76127774e3e59557e605fb5b4e8804f8d2c0ef03dd0eabe8de48df0fe49f0ca3
  gitRevision: 5a0d398fa30c074b529a9a655cf3f07370fc6dc7
  verdict: reproducible
- date: 2022-05-08
  version: 3.8.0
  appHash: 800949a6583a347102ba33bbcfe337ae6193892e2a956acbec75444492ad8b3c
  gitRevision: edb4930bbfa185f9457134b8514a962a71f5e340
  verdict: reproducible
- date: 2022-02-25
  version: 3.7.9
  appHash: 3d377177d77c6af84720fff05fe62aad6661b81f21ee5b4a6db490ac6b27a6c5
  gitRevision: b87bc633a27569f90a012614c792e1a3c6d400c6
  verdict: reproducible
- date: 2022-01-24
  version: 3.7.8
  appHash: c1dc700b749152fefd871aa830caffc2a1fc85fed7194f0a114fd1c7f4bfeb74
  gitRevision: 6c1ab1bec6c85a048a8e62c6f33fa74a84510d42
  verdict: reproducible
- date: 2021-12-27
  version: 3.7.7
  appHash: 714f02f6cac34bddd8d71dd8c836501f30dce59080b7222f801a83e12b49d45d
  gitRevision: e2527b5b894b8e72c53b5db5662abd71db4e2903
  verdict: reproducible
- date: 2021-11-14
  version: 3.7.6
  appHash: 5e9bc665f0dcb2bc3c040e50f63c6eaf4962f6f3c00148f7143b9e25fa3fc7ca
  gitRevision: 56fb701c15d8bd888ec328bc938ceea06621175f
  verdict: reproducible
- date: 2021-10-29
  version: 3.7.5
  appHash: 4bc2491a7c50c241e12ff3f5ec1d84e82d45d9fa86c41e4db1a8869ad2b77951
  gitRevision: 9a4ffd99428ebf9a8135f53771d4aa977bc9b837
  verdict: reproducible
- date: 2021-10-04
  version: 3.7.3
  appHash: f43452d74790aa086fbd2c73718e81a0fc24cb238258fda67cd5602193236e2f
  gitRevision: 2d2841809c0646bb07b9f3f00afff8e711f72e99
  verdict: reproducible
- date: 2021-09-17
  version: 3.7.0
  appHash: 55c962cc12defa44ef90cd10f2a10dc574b368c364ac9ed7ea910515b0a05996
  gitRevision: 0b717ae8ed6ffcd503e2dd53ab4953a4e003fc6f
  verdict: reproducible
- date: 2021-08-03
  version: 3.6.4
  appHash: 9a796e5b8986c727e0cd112899c40cdd832c94805aa9a547a7daf95ec5ec9dc5
  gitRevision: 53ccc17be59a9ee81c709dc4d57e0215046ecb57
  verdict: reproducible
- date: 2021-07-13
  version: 3.6.3
  appHash: 6779507d1ad1da738312c43fbe6380f6d3e8947d66cd5d89de0fe62fc242217b
  gitRevision: 6af0e73625f44d1f6cd9230b1e2b6eff28d71719
  verdict: reproducible
- date: 2021-06-04
  version: 3.6.0
  appHash: e93bcf3bcad8b84568a3101c4a87b9c9bb684c7c544fc6b05f204d9fa5fbb57d
  gitRevision: b754fe651fcfdf446b444bf92cd92864316a7b57
  verdict: reproducible
- date: 2021-05-15
  version: 3.5.9
  appHash: 76cc3df154ff6d47b5366a328515cf14c1d550ca7d71063d851c1949324ef4fa
  gitRevision: f19096afe2eb9df9f5be796428c76a142d2bb2eb
  verdict: nonverifiable
- date: 2021-05-03
  version: 3.5.9
  appHash: f62c0b4b4882ad15a561fbabcccc34739d66470e752a21a4eab4037736593476
  gitRevision: 13d8e0095e0944d8d255811487d819fafc74c5e1
  verdict: nonverifiable
- date: 2021-05-03
  version: 3.5.8
  appHash: 950446b62e9e1a536a2341155949750b9856a24768374aac3ce74f2e91394356
  gitRevision: a393a14039f9ee960578b19999c30df46191dd01
  verdict: reproducible
- date: 2021-04-01
  version: 3.5.4
  appHash: 4ed9729881676b84d7ed65b0f0bd583c11c465186e896e96888c5d323e8c5002
  gitRevision: 512303fa6c495727005df2cb8e1c853128ee03ca
  verdict: reproducible
- date: 2021-02-05
  version: 3.4.9
  appHash: fd146d68348e332a6a6e2f548c473599ba684cbb6107328a3871de09259f00e5
  gitRevision: 58e8db9ca9f5b5ec1a1881ad5a74fb402bcf3438
  verdict: reproducible
- date: 2021-01-18
  version: 3.4.8
  appHash: ef539fe60af20a538eb4bdaad37a8cde12cb873cca97800749a71a5add0e9ff7
  gitRevision: 8eb6570934dad87e4b32ae8c8d7e6f06ed8aae43
  verdict: reproducible
- date: 2021-01-03
  version: 3.4.5
  appHash: efa5e3e56b1081bb66ca99d3fea7d5b1375a8f30696acf0938a324ba12c5458c
  gitRevision: 6f2db0822de5b16cad4f1a7459068a27a50c4896
  verdict: reproducible
- date: 2020-12-15
  version: 3.4.4
  appHash: d54f84856c25c302978ed5c23ad01c3c0c89930f8f9cd2098558563d9f8b1a3e
  gitRevision: 9817359e09ab4cc05136dc7dfbb40d950750ec4f
  verdict: reproducible
- date: 2020-11-08
  version: 3.4.2
  appHash: e631aef67a2d50cced4f3a2a850e6f32950e0a91e12678678441defa3da71681
  gitRevision: e855e82e36403f60cfebfd66a8126f9c7dc1cfd4
  verdict: reproducible
- date: 2020-10-17
  version: 3.4.1
  appHash: 991b1d5672faed19ee8e96a66f7b6812e23971eaf28187424c9af41c4ff16d82
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-10-07
  version: 3.4.0
  appHash: fb7d9611ad878ef4116b525a50255f3b16725ec673a5c717f14c5d021b242188
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-08-31
  version: 3.3.9
  appHash: 6a4a4bb05c0c087c4b85486f01982a8bf1bde91a70c587e22929e9faed3eb6ed
  gitRevision: ea0cf3403d57a0e33f7d7627d9854b737fc0d62e
  verdict: nonverifiable
- date: 2020-07-11
  version: 3.3.8
  appHash: 3a0a02ea8ccd791ab3ec24bac4d45249164f5c53366538b5befcbd4df3f6edb3
  gitRevision: 09ea9943f4ad41f83d28027e3275105483849996
  verdict: reproducible
- date: 2020-05-06
  version: 3.3.7
  appHash: 847a67a5cfa498cf2e137b0a4306202322a35d4e3fba6bb90a269709e26e11ab
  gitRevision: 4392a6481b289b2cb82d790db15fa8feadf40b1d
  verdict: reproducible
- date: 2020-04-26
  version: 3.3.6
  appHash: b91e3c6e35aa9223c7d1f62498b162fde226db38049016a354f87578fde371ab
  gitRevision: 9cc435e13b5b513f7ecdaa1baee739b2683d2ba5
  verdict: reproducible
- date: 2020-03-14
  version: 3.3.5
  appHash: e30092950197aa2801b0f958a90496cf182f76e790f2d7e82e08dbe01b7c32c8
  gitRevision: a6b2771dbc314160ba304573fd0a6cc5d6d1ccb9
  verdict: reproducible
- date: 2020-02-17
  version: 3.3.4
  appHash: f88686a2e41718b82ba8d2f5ff7eb8d0ada044d29711e75f0128104bbee40baf
  gitRevision: bb658ba9291af3d814ba8a00c4726c9584e379d1
  verdict: reproducible
- date: 2020-01-18
  version: 3.3.2
  appHash: 7e48e2ff0e8d484f4000b7d96bbdc6b0939a76d8ca80355b5ebedbf68511f77c
  gitRevision: d47e69dd99ac700665328b92b3026a2cf6e36960
  verdict: reproducible
- date: 2020-01-09
  version: 3.3.0
  appHash: b2e3f2d437bba5d97f3b331aac20616d3312e34d25023c38c42483974828cdec
  gitRevision: 0d558ec280ca9606901f6557622af98e0cbdc97b
  verdict: reproducible
- date: 2019-11-23
  version: 3.2.7
  appHash: 8b2e67fc333eeef5b10ce6f9f5fc3e4ca104c1eca9c539b73805276e09d838db
  gitRevision: 3d972d9773b0fd2fb1602d31117a50be01d48610
  verdict: reproducible
twitter: Blockstream
social:
- https://www.linkedin.com/company/blockstream
- https://www.facebook.com/Blockstream
redirect_from:
- /greenwallet/
- /com.greenaddress.greenbits_android_wallet/
- /posts/2019/11/greenwallet/
- /posts/com.greenaddress.greenbits_android_wallet/
developerName: GreenAddress IT Ltd
features: 

---

**Update 2023-11-26**: The supposedly correct code was pushed hours after
[this tweet]().
We ran the script and this time got this result:

```
===== Begin Results =====
appId:          com.greenaddress.greenbits_android_wallet
signer:         32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
apkVersionName: 4.0.20
apkVersionCode: 22000420
verdict:        
appHash:        12843c2f7714244eec94a885094ccab634f1561c1458ed3194c236ba1f1ab8ee
commit:         1d7e01690f0f19368c24e96b005916dca8814a17

Diff:
Files /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000420/assets/dexopt/baseline.prof and /tmp/fromBuild_com.greenaddress.greenbits_android_wallet_22000420/assets/dexopt/baseline.prof differ
Files /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000420/classes.dex and /tmp/fromBuild_com.greenaddress.greenbits_android_wallet_22000420/classes.dex differ
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000420/META-INF: GREENADD.RSA
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000420/META-INF: GREENADD.SF
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000420/META-INF: MANIFEST.MF

Revision, tag (and its signature):

===== End Results =====
```

Especially the diff in `classes.dex` is concerning and more than the prior
version's diff. The diffoscope output is also gigantic with hundreds of diffs
like this one:

```
│ │ ├── com/blockstream/green/di/KoinKt.class
│ │ │ ├── procyon -ec {}
│ │ │ │ @@ -18,22 +18,22 @@
│ │ │ │  
│ │ │ │  public abstract class KoinKt
│ │ │ │  {
│ │ │ │      public static final void startKoin(final Context context) {
│ │ │ │          Intrinsics.checkNotNullParameter((Object)context, "context");
│ │ │ │          final String absolutePath = context.getFilesDir().getAbsolutePath();
│ │ │ │          Intrinsics.checkNotNullExpressionValue((Object)absolutePath, "getAbsolutePath(...)");
│ │ │ │ -        final String s = "MVowd1VIVVBaUWd1NDlYVW5YK1lvVllmY0RoR2pmdjJCR29zbDFtWG4zND0=";
│ │ │ │ -        final String s2 = "LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tDQpNSUdIQWdFQU1CTUdCeXFHU000OUFnRUdDQ3FHU000OUF3RUhCRzB3YXdJQkFRUWc1dFAzL05mTld0R2h4ZTlyDQo1T1diQzU0OVNIeS93NFEvZG14bTVjWWZBQnFoUkFOQ0FBUTVXaC9seW1KSUprR0c2bFNJYlVDS01WZGZUbjE3DQp6TWZ2RkdBWlVNUHo5MzBnZE55c0doUkhod3dBdUJ1UWlGdExSaXRLZzlUNXp6MjRBTDlTVnBNeQ0KLS0tLS1FTkQgUFJJVkFURSBLRVktLS0tLQ==";
│ │ │ │ -        final String s3 = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlDcGpDQ0FrMmdBd0lCQWdJVVRZTDd3R0RhMmtvaW91eUxmTzROaW9UNjRyMHdDZ1lJS29aSXpqMEVBd0l3DQpnWU14Q3pBSkJnTlZCQVlUQWxWVE1STXdFUVlEVlFRSUV3cERZV3hwWm05eWJtbGhNUll3RkFZRFZRUUhFdzFUDQpZVzRnUm5KaGJtTnBjMk52TVJRd0VnWURWUVFLRXd0Q2JHOWphM04wY21WaGJURWRNQnNHQTFVRUN4TVVRMlZ5DQpkR2xtYVdOaGRHVkJkWFJvYjNKcGRIa3hFakFRQmdOVkJBTVRDVWRNSUM5MWMyVnljekFlRncweU16QTBNRE13DQpPRE01TURCYUZ3MHpNekF6TXpFd09ETTVNREJhTUlHSk1Rc3dDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LDQpRMkZzYVdadmNtNXBZVEVXTUJRR0ExVUVCeE1OVTJGdUlFWnlZVzVqYVhOamJ6RVVNQklHQTFVRUNoTUxRbXh2DQpZMnR6ZEhKbFlXMHhIVEFiQmdOVkJBc1RGRU5sY25ScFptbGpZWFJsUVhWMGFHOXlhWFI1TVJnd0ZnWURWUVFEDQpFdzlIVENBdmRYTmxjbk12WjNKbFpXNHdXVEFUQmdjcWhrak9QUUlCQmdncWhrak9QUU1CQndOQ0FBUTVXaC9sDQp5bUpJSmtHRzZsU0liVUNLTVZkZlRuMTd6TWZ2RkdBWlVNUHo5MzBnZE55c0doUkhod3dBdUJ1UWlGdExSaXRLDQpnOVQ1enoyNEFMOVNWcE15bzRHV01JR1RNQTRHQTFVZER3RUIvd1FFQXdJQnBqQWRCZ05WSFNVRUZqQVVCZ2dyDQpCZ0VGQlFjREFRWUlLd1lCQlFVSEF3SXdEQVlEVlIwVEFRSC9CQUl3QURBZEJnTlZIUTRFRmdRVVVQZDV4aVdWDQpzVTZsd3RqbVZUbDh4aVFxUmNrd0h3WURWUjBqQkJnd0ZvQVVUUTczRjFNTWJwT2l4QnRkUG9vUEpnYkRKWlF3DQpGQVlEVlIwUkJBMHdDNElKYkc5allXeG9iM04wTUFvR0NDcUdTTTQ5QkFNQ0EwY0FNRVFDSUhLQTRzRHZwMjRIDQo3QjBOZTl0OEc1d1lyQnBuQms2WFBiMWFzNG40TW5xUUFpQXJRcmFRSUc1U2pVR3lSUkpBRis2Z1JtN01IajFRDQo4M2FFbWNGVy9KNld6QT09DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tDQotLS0tLUJFR0lOIENFUlRJRklDQVRFLS0tLS0NCk1JSUNpakNDQWpHZ0F3SUJBZ0lVSjA2c3lZQjFUUFJiU21URDRVQ3BUMFBtQStVd0NnWUlLb1pJemowRUF3SXcNCmZqRUxNQWtHQTFVRUJoTUNWVk14RXpBUkJnTlZCQWdUQ2tOaGJHbG1iM0p1YVdFeEZqQVVCZ05WQkFjVERWTmgNCmJpQkdjbUZ1WTJselkyOHhGREFTQmdOVkJBb1RDMEpzYjJOcmMzUnlaV0Z0TVIwd0d3WURWUVFMRXhSRFpYSjANCmFXWnBZMkYwWlVGMWRHaHZjbWwwZVRFTk1Bc0dBMVVFQXhNRVIwd2dMekFlRncweU1UQTBNall4TnpFME1EQmENCkZ3MHpNVEEwTWpReE56RTBNREJhTUlHRE1Rc3dDUVlEVlFRR0V3SlZVekVUTUJFR0ExVUVDQk1LUTJGc2FXWnYNCmNtNXBZVEVXTUJRR0ExVUVCeE1OVTJGdUlFWnlZVzVqYVhOamJ6RVVNQklHQTFVRUNoTUxRbXh2WTJ0emRISmwNCllXMHhIVEFiQmdOVkJBc1RGRU5sY25ScFptbGpZWFJsUVhWMGFHOXlhWFI1TVJJd0VBWURWUVFERXdsSFRDQXYNCmRYTmxjbk13V1RBVEJnY3Foa2pPUFFJQkJnZ3Foa2pPUFFNQkJ3TkNBQVRXbE5pKzlQOFpkUmZhUDFWT09NYjkNCmUrVlN1Z0R4d3ZONDFaVGRxNWFRMXlUWEh4MmZjTXlvd29EYVNDQmc0NHJ6UEovVERPcklIMldXV0NhSG1IZ1QNCm80R0dNSUdETUE0R0ExVWREd0VCL3dRRUF3SUJwakFkQmdOVkhTVUVGakFVQmdnckJnRUZCUWNEQVFZSUt3WUINCkJRVUhBd0l3RWdZRFZSMFRBUUgvQkFnd0JnRUIvd0lCQXpBZEJnTlZIUTRFRmdRVVRRNzNGMU1NYnBPaXhCdGQNClBvb1BKZ2JESlpRd0h3WURWUjBqQkJnd0ZvQVV6cUZyNmp2bHgzYmxadFlhcGNaSFZZcE9LU013Q2dZSUtvWkkNCnpqMEVBd0lEUndBd1JBSWdKdmdKOGVoS3gwVmVuTXlVVC9NUlhsbUNsQVJjMU5wMzkvRmJwNEdJYmQ4Q0lHaGsNCk1LVmNEQTVpdVFaN3hoWlUxUzhQT2gxTDl1VDM1VWtFNyt4bUdOanINCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0NCi0tLS0tQkVHSU4gQ0VSVElGSUNBVEUtLS0tLQ0KTUlJQ1lqQ0NBZ2lnQXdJQkFnSVVERXcyb3NOQnIrSDFvNFdDdlBTUklqTnpVelF3Q2dZSUtvWkl6ajBFQXdJdw0KZmpFTE1Ba0dBMVVFQmhNQ1ZWTXhFekFSQmdOVkJBZ1RDa05oYkdsbWIzSnVhV0V4RmpBVUJnTlZCQWNURFZOaA0KYmlCR2NtRnVZMmx6WTI4eEZEQVNCZ05WQkFvVEMwSnNiMk5yYzNSeVpXRnRNUjB3R3dZRFZRUUxFeFJEWlhKMA0KYVdacFkyRjBaVUYxZEdodmNtbDBlVEVOTUFzR0ExVUVBeE1FUjB3Z0x6QWVGdzB5TVRBME1qWXhOekUwTURCYQ0KRncwek1UQTBNalF4TnpFME1EQmFNSDR4Q3pBSkJnTlZCQVlUQWxWVE1STXdFUVlEVlFRSUV3cERZV3hwWm05eQ0KYm1saE1SWXdGQVlEVlFRSEV3MVRZVzRnUm5KaGJtTnBjMk52TVJRd0VnWURWUVFLRXd0Q2JHOWphM04wY21WaA0KYlRFZE1Cc0dBMVVFQ3hNVVEyVnlkR2xtYVdOaGRHVkJkWFJvYjNKcGRIa3hEVEFMQmdOVkJBTVRCRWRNSUM4dw0KV1RBVEJnY3Foa2pPUFFJQkJnZ3Foa2pPUFFNQkJ3TkNBQVRwODNrNFNxUTVnZUdScElwRHVVMjB2clp6OHFKOA0KZUJEWWJXM25JbEM4VU0vUHpWQlNOQS9NcVdsQWFtQjNZR0srVmxnc0VNYmVPVVdFTTRjOXp0VmxvMlF3WWpBTw0KQmdOVkhROEJBZjhFQkFNQ0FhWXdIUVlEVlIwbEJCWXdGQVlJS3dZQkJRVUhBd0VHQ0NzR0FRVUZCd01DTUJJRw0KQTFVZEV3RUIvd1FJTUFZQkFmOENBUU13SFFZRFZSME9CQllFRk02aGErbzc1Y2QyNVdiV0dxWEdSMVdLVGlrag0KTUFvR0NDcUdTTTQ5QkFNQ0EwZ0FNRVVDSUdCa2p5cDFOZC9tL2IzakVBVW14QWlzcUNhaHVRVVB1eVFySXdvMA0KWkYvOUFpRUFzWjhxWmZrVVpIMllhN3k2Y2NGVERwcy9haHNGV1NyUmFvOHJ1M3loaHJzPQ0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQ==";
│ │ │ │ +        final String s = "";
│ │ │ │ +        final String s2 = "";
│ │ │ │ +        final String s3 = "";
│ │ │ │          final boolean boolean1 = context.getResources().getBoolean(R$bool.feature_analytics);
│ │ │ │          final boolean boolean2 = context.getResources().getBoolean(R$bool.feature_lightning);
│ │ │ │          final int n = 1;
│ │ │ │          int n2;
│ │ │ │ -        if (boolean2 && ((StringsKt.isBlank((CharSequence)"MVowd1VIVVBaUWd1NDlYVW5YK1lvVllmY0RoR2pmdjJCR29zbDFtWG4zND0=") ? 1 : 0) ^ n)) {
│ │ │ │ +        if (boolean2 && ((StringsKt.isBlank((CharSequence)"") ? 1 : 0) ^ n)) {
```

`s3` appears to be multiple certificates base64 encoded:

```
-----BEGIN CERTIFICATE-----
MIICpjCCAk2gAwIBAgIUTYL7wGDa2koiouyLfO4NioT64r0wCgYIKoZIzj0EAwIw
gYMxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1T
YW4gRnJhbmNpc2NvMRQwEgYDVQQKEwtCbG9ja3N0cmVhbTEdMBsGA1UECxMUQ2Vy
dGlmaWNhdGVBdXRob3JpdHkxEjAQBgNVBAMTCUdMIC91c2VyczAeFw0yMzA0MDMw
ODM5MDBaFw0zMzAzMzEwODM5MDBaMIGJMQswCQYDVQQGEwJVUzETMBEGA1UECBMK
Q2FsaWZvcm5pYTEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzEUMBIGA1UEChMLQmxv
Y2tzdHJlYW0xHTAbBgNVBAsTFENlcnRpZmljYXRlQXV0aG9yaXR5MRgwFgYDVQQD
Ew9HTCAvdXNlcnMvZ3JlZW4wWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAAQ5Wh/l
ymJIJkGG6lSIbUCKMVdfTn17zMfvFGAZUMPz930gdNysGhRHhwwAuBuQiFtLRitK
g9T5zz24AL9SVpMyo4GWMIGTMA4GA1UdDwEB/wQEAwIBpjAdBgNVHSUEFjAUBggr
BgEFBQcDAQYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUUPd5xiWV
sU6lwtjmVTl8xiQqRckwHwYDVR0jBBgwFoAUTQ73F1MMbpOixBtdPooPJgbDJZQw
FAYDVR0RBA0wC4IJbG9jYWxob3N0MAoGCCqGSM49BAMCA0cAMEQCIHKA4sDvp24H
7B0Ne9t8G5wYrBpnBk6XPb1as4n4MnqQAiArQraQIG5SjUGyRRJAF+6gRm7MHj1Q
83aEmcFW/J6WzA==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIICijCCAjGgAwIBAgIUJ06syYB1TPRbSmTD4UCpT0PmA+UwCgYIKoZIzj0EAwIw
fjELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
biBGcmFuY2lzY28xFDASBgNVBAoTC0Jsb2Nrc3RyZWFtMR0wGwYDVQQLExRDZXJ0
aWZpY2F0ZUF1dGhvcml0eTENMAsGA1UEAxMER0wgLzAeFw0yMTA0MjYxNzE0MDBa
Fw0zMTA0MjQxNzE0MDBaMIGDMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZv
cm5pYTEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzEUMBIGA1UEChMLQmxvY2tzdHJl
YW0xHTAbBgNVBAsTFENlcnRpZmljYXRlQXV0aG9yaXR5MRIwEAYDVQQDEwlHTCAv
dXNlcnMwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATWlNi+9P8ZdRfaP1VOOMb9
e+VSugDxwvN41ZTdq5aQ1yTXHx2fcMyowoDaSCBg44rzPJ/TDOrIH2WWWCaHmHgT
o4GGMIGDMA4GA1UdDwEB/wQEAwIBpjAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYB
BQUHAwIwEgYDVR0TAQH/BAgwBgEB/wIBAzAdBgNVHQ4EFgQUTQ73F1MMbpOixBtd
PooPJgbDJZQwHwYDVR0jBBgwFoAUzqFr6jvlx3blZtYapcZHVYpOKSMwCgYIKoZI
zj0EAwIDRwAwRAIgJvgJ8ehKx0VenMyUT/MRXlmClARc1Np39/Fbp4GIbd8CIGhk
MKVcDA5iuQZ7xhZU1S8POh1L9uT35UkE7+xmGNjr
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIICYjCCAgigAwIBAgIUDEw2osNBr+H1o4WCvPSRIjNzUzQwCgYIKoZIzj0EAwIw
fjELMAkGA1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExFjAUBgNVBAcTDVNh
biBGcmFuY2lzY28xFDASBgNVBAoTC0Jsb2Nrc3RyZWFtMR0wGwYDVQQLExRDZXJ0
aWZpY2F0ZUF1dGhvcml0eTENMAsGA1UEAxMER0wgLzAeFw0yMTA0MjYxNzE0MDBa
Fw0zMTA0MjQxNzE0MDBaMH4xCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9y
bmlhMRYwFAYDVQQHEw1TYW4gRnJhbmNpc2NvMRQwEgYDVQQKEwtCbG9ja3N0cmVh
bTEdMBsGA1UECxMUQ2VydGlmaWNhdGVBdXRob3JpdHkxDTALBgNVBAMTBEdMIC8w
WTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATp83k4SqQ5geGRpIpDuU20vrZz8qJ8
eBDYbW3nIlC8UM/PzVBSNA/MqWlAamB3YGK+VlgsEMbeOUWEM4c9ztVlo2QwYjAO
BgNVHQ8BAf8EBAMCAaYwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMBIG
A1UdEwEB/wQIMAYBAf8CAQMwHQYDVR0OBBYEFM6ha+o75cd25WbWGqXGR1WKTikj
MAoGCCqGSM49BAMCA0gAMEUCIGBkjyp1Nd/m/b3jEAUmxAisqCahuQUPuyQrIwo0
ZF/9AiEAsZ8qZfkUZH2Ya7y6ccFTDps/ahsFWSrRao8ru3yhhrs=
-----END CERTIFICATE-----
```

but thousands of lines like these:

```
│ │ -3a138a: 1a02 8623                              |004b: const-string v2, ".code" // string@2386
│ │ -3a138e: 7130 1300 4102                         |004d: invoke-static {v1, v4, v2}, La/b;.j:(Ljava/lang/String;Lbreez_sdk/RustCallStatus;Ljava/lang/String;)Ljava/lang/String; // method@0013
│ │ -3a1394: 0c04                                   |0050: move-result-object v4
│ │ -3a1396: 7020 db98 4000                         |0051: invoke-direct {v0, v4}, Lbreez_sdk/InternalException;.<init>:(Ljava/lang/String;)V // method@98db
```

are a bit harder to make sense of.

This product is **not verifiable**.

Prior this was the result:

With this {% include testScript.html %} we get:

```
...
+ git clone --quiet --branch release_4.0.20 --depth 1 https://github.com/Blockstream/green_android/ app
warning: Could not find remote branch release_4.0.20 to clone.
fatal: Remote branch release_4.0.20 not found in upstream origin
+ exit 1
```

In the best case the provider forgot to tag the released source code and we can
just compile the latest version and confirm its reproducibility but in this
case, the source code is missing. Checking
[recent commits](https://github.com/Blockstream/green_android/commits/master)
we see
[Increment to version 4.0.19](https://github.com/Blockstream/green_android/commit/5839c9b7cd28eec6c5992715df28adf33de0822a)
as the last commit and it's not a typo. This commit sets the version to 4.0.19
and the file we got from Google Play was 4.0.20. This version is
**not verifiable**!

For the record, the file we have here has the sha256 hash
`12843c2f7714244eec94a885094ccab634f1561c1458ed3194c236ba1f1ab8ee`.
