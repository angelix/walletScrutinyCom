---
wsId: swanBitcoin
title: Swan Bitcoin
altTitle: 
authors:
- danny
users: 10000
appId: com.swanbitcoin.android
appCountry: 
released: 2022-03-25
updated: 2023-11-21
version: 1.4.7
stars: 4.6
ratings: 
reviews: 64
size: 
website: https://www.swanbitcoin.com
repository: 
issue: 
icon: com.swanbitcoin.android.png
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: swan
social:
- https://www.instagram.com/swanbitcoin
- https://www.linkedin.com/company/swanbitcoin
redirect_from: 
developerName: Swan Bitcoin
features: 

---

## App Description from Google Play

> Buy Bitcoin anytime - purchases are instantly added to your Bitcoin balance.
>
> Create a Bitcoin savings plan for set-it-and-forget-it accumulation.

## Analysis

- [Custody](https://www.swanbitcoin.com/security/) is with Swan's partner, Fortress.
- This is a **custodial** provider.
