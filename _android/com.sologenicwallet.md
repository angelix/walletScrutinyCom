---
wsId: 
title: SOLO DEX
altTitle: 
authors: 
users: 50000
appId: com.sologenicwallet
appCountry: 
released: 2020-02-04
updated: 2023-09-20
version: 3.0.6
stars: 4.3
ratings: 3701
reviews: 133
size: 
website: https://www.sologenic.com/
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2020-06-20
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.sologenicwallet/
- /posts/com.sologenicwallet/
developerName: Sologenic
features: 

---

This wallet does not support BTC.
