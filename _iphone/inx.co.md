---
wsId: iNXCrypto
title: INX Crypto
altTitle: 
authors:
- danny
appId: inx.co
appCountry: us
idd: '1601656456'
released: 2022-06-11
updated: 2023-02-12
version: 2.0.5
stars: 4.6
reviews: 11
size: '37054464'
website: 
repository: 
issue: 
icon: inx.co.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-30
signer: 
reviewArchive: 
twitter: INX_Group
social:
- https://www.inx.co
- https://www.linkedin.com/company/theinxdigitalcompany
- https://www.facebook.com/INXLtd
- https://www.youtube.com/channel/UCgk9PJ_3NvCA-dIJ5PXNJeg
- https://t.me/INXCommunity
- https://www.reddit.com/r/INX_Ltd
- https://discord.com/invite/A5KkK4EAV7
features: 
developerName: INX Limited

---

{% include copyFromAndroid.html %}
