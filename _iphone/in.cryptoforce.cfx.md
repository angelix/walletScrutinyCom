---
wsId: cryptoforceTrade
title: Cryptoforce:Trade|Stake Crypto
altTitle: 
authors:
- danny
appId: in.cryptoforce.cfx
appCountry: in
idd: '6444105412'
released: 2023-01-24
updated: 2023-07-17
version: 1.2.0
stars: 5
reviews: 16
size: '33926144'
website: 
repository: 
issue: 
icon: in.cryptoforce.cfx.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-18
signer: 
reviewArchive: 
twitter: cryptoforce_in
social:
- https://www.facebook.com/cryptoforceindia
- https://www.instagram.com/cryptoforce_in
- https://www.linkedin.com/company/cryptoforcein
- https://t.me/Cryptoforceofficial
- https://cryptoforceindia.medium.com
features: 
developerName: 'CryptoForce: Bitcoin & Cryptocurrency Investment'

---

{% include copyFromAndroid.html %}
