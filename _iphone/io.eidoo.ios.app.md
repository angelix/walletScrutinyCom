---
wsId: eidooApp
title: Eidoo
altTitle: 
authors:
- danny
appId: io.eidoo.ios.app
appCountry: it
idd: '1626030540'
released: 2022-07-21
updated: 2023-07-21
version: 1.14.2
stars: 4.1
reviews: 15
size: '58854400'
website: https://eidoo.app
repository: 
issue: 
icon: io.eidoo.ios.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-08
signer: 
reviewArchive: 
twitter: eidoo_io
social:
- https://t.me/eidoocryptoworld
features: 
developerName: Eidoo Tech LTD

---

{% include copyFromAndroid.html %}
