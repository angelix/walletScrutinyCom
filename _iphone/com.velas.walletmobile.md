---
wsId: VelasWallet
title: Velas Mobile Wallet
altTitle: 
authors:
- danny
- emanuel
- leo
appId: com.velas.walletmobile
appCountry: us
idd: 1541032748
released: 2020-12-12
updated: 2023-04-12
version: 2.3.10
stars: 2.2
reviews: 10
size: '32556032'
website: https://velas.com
repository: 
issue: 
icon: com.velas.walletmobile.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-10-31
signer: 
reviewArchive: 
twitter: velasblockchain
social:
- https://www.linkedin.com/company/velas-ag
- https://www.facebook.com/velasblockchain
features: 
developerName: Velas

---

{% include copyFromAndroid.html %}
