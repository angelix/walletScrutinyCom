---
wsId: coinZoom
title: 'CoinZoom: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.ios.coinzoomsimple
appCountry: us
idd: '1575983875'
released: 2022-01-21
updated: 2023-09-21
version: 1.1.2
stars: 4.5
reviews: 749
size: '118500352'
website: http://www.coinzoom.com
repository: 
issue: 
icon: com.ios.coinzoomsimple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-30
signer: 
reviewArchive: 
twitter: GetCoinZoom
social:
- https://www.facebook.com/CoinZoom
- https://www.linkedin.com/company/coinzoomhq/
features: 
developerName: CoinZoom

---

{% include copyFromAndroid.html %}
