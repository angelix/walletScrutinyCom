---
wsId: bitstampCrypto
title: 'Bitstamp: Buy Crypto Simply'
altTitle: 
authors:
- danny
appId: net.bitstamp.simple
appCountry: us
idd: '1494703801'
released: 2023-07-20
updated: 2023-11-20
version: '1.6'
stars: 3.7
reviews: 3
size: '114763776'
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.simple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-14
signer: 
reviewArchive: 
twitter: Bitstamp
social:
- https://www.linkedin.com/company/bitstamp
- https://www.facebook.com/Bitstamp
- https://www.instagram.com/bitstampexchange
features: 
developerName: Bitstamp Ltd.

---

{% include copyFromAndroid.html %}