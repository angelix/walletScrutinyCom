---
wsId: keyApp
title: 'Key App: send & receive USDC'
altTitle: 
authors:
- danny
appId: org.p2p.cyber
appCountry: CY
idd: '1605603333'
released: 2022-02-14
updated: 2023-08-04
version: 2.9.0
stars: 5
reviews: 4
size: '124850176'
website: https://key.app
repository: 
issue: 
icon: org.p2p.cyber.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-05-06
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: P2P Wallet

---

{% include copyFromAndroid.html %}
