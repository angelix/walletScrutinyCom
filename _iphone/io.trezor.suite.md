---
wsId: trezorSuiteLite
title: Trezor Suite Lite
altTitle: 
authors:
- danny
appId: io.trezor.suite
appCountry: us
idd: '1631884497'
released: 2023-05-14
updated: 2023-11-22
version: 23.11.1
stars: 3.2
reviews: 25
size: '41640960'
website: https://trezor.io/
repository: 
issue: 
icon: io.trezor.suite.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-18
signer: 
reviewArchive: 
twitter: trezor
social:
- https://www.reddit.com/r/TREZOR
- https://www.instagram.com/trezor.io
- https://www.facebook.com/trezor.io
features: 
developerName: Trezor Company s.r.o.

---

{% include copyFromAndroid.html %}
