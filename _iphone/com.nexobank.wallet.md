---
wsId: nexo
title: 'Nexo: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2023-11-16
version: 3.8.3
stars: 3.8
reviews: 1157
size: '92802048'
website: https://nexo.com
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive: 
twitter: NexoFinance
social:
- https://www.facebook.com/nexofinance
- https://www.reddit.com/r/Nexo
features: 
developerName: Nexo Capital Inc.

---

{% include copyFromAndroid.html %}
