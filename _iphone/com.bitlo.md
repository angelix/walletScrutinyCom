---
wsId: bitlo
title: 'Bitlo: Bitcoin & Kripto Para'
altTitle: 
authors:
- danny
appId: com.bitlo
appCountry: tr
idd: '1544492069'
released: 2020-12-28
updated: 2023-11-02
version: 2.1.5
stars: 3.3
reviews: 289
size: '60584960'
website: https://www.bitlo.com/
repository: 
issue: 
icon: com.bitlo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-02-08
signer: 
reviewArchive: 
twitter: bitlocom
social:
- https://www.linkedin.com/company/bitlo/
features: 
developerName: Bitlo Teknoloji Anonim Şirketi

---

{% include copyFromAndroid.html %}

