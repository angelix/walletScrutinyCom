---
wsId: swanBitcoin
title: Swan Bitcoin
altTitle: 
authors:
- danny
appId: com.swanbitcoin.app
appCountry: us
idd: '1576287352'
released: 2022-08-25
updated: 2023-11-22
version: 1.4.7
stars: 4.8
reviews: 509
size: '115904512'
website: http://www.swanbitcoin.com
repository: 
issue: 
icon: com.swanbitcoin.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-17
signer: 
reviewArchive: 
twitter: swan
social:
- https://www.instagram.com/swanbitcoin
- https://www.linkedin.com/company/swanbitcoin
features: 
developerName: Swan Bitcoin

---

{% include copyFromAndroid.html %}
