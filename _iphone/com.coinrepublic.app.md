---
wsId: coinRepublic
title: Coin Republic
altTitle: 
authors:
- danny
appId: com.coinrepublic.app
appCountry: au
idd: '1540941971'
released: 2020-11-28
updated: 2023-11-19
version: '1.12'
stars: 5
reviews: 2
size: '26174464'
website: https://coinrepublic.exchange/
repository: 
issue: 
icon: com.coinrepublic.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-17
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: COIN REPUBLIC PTY LTD

---

{% include copyFromAndroid.html %}

