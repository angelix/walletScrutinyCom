---
wsId: wirex
title: 'Wirex: All-In-One Crypto App'
altTitle: 
authors:
- danny
appId: com.wirex
appCountry: us
idd: 1090004654
released: 2016-03-22
updated: 2023-11-08
version: 3.45.30
stars: 3.6
reviews: 625
size: '206886912'
website: https://wirexapp.com/
repository: 
issue: 
icon: com.wirex.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-10
signer: 
reviewArchive: 
twitter: wirexapp
social:
- https://www.linkedin.com/company/wirex-limited
- https://www.facebook.com/wirexapp
features: 
developerName: Wirex Limited

---

{% include copyFromAndroid.html %}
