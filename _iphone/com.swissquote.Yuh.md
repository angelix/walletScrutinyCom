---
wsId: yuh
title: 'Yuh: 3-in-1 Finance App'
altTitle: 
authors:
- danny
appId: com.swissquote.Yuh
appCountry: ch
idd: '1493935010'
released: 2021-05-10
updated: 2023-11-20
version: 1.17.1
stars: 4.7
reviews: 9218
size: '125345792'
website: https://www.yuh.com
repository: 
issue: 
icon: com.swissquote.Yuh.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-02
signer: 
reviewArchive: 
twitter: yuh_app
social:
- https://www.facebook.com/yuhapp.en/
features: 
developerName: Swissquote

---

{% include copyFromAndroid.html %}
