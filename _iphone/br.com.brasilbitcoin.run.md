---
wsId: brasilBitcoin
title: 'Brasil Bitcoin: Cripto Grátis'
altTitle: 
authors:
- danny
appId: br.com.brasilbitcoin.run
appCountry: br
idd: 1519300849
released: 2020-07-27
updated: 2023-10-01
version: 3.0.02
stars: 4.7
reviews: 2107
size: '84090880'
website: 
repository: 
issue: 
icon: br.com.brasilbitcoin.run.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: brbtcoficial
social:
- https://www.facebook.com/brbtcoficial
features: 
developerName: Brasil Bitcoin Servicos Digitais LTDA

---

{% include copyFromAndroid.html %}
