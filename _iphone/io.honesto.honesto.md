---
wsId: honestoTrading
title: Honesto - Trading App
altTitle: 
authors:
- danny
appId: io.honesto.honesto
appCountry: ch
idd: '1503465220'
released: 2021-12-09
updated: 2023-11-15
version: 2.0.2
stars: 4.8
reviews: 33
size: '104165376'
website: 
repository: 
issue: 
icon: io.honesto.honesto.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Honesto AG

---

{% include copyFromAndroid.html %}