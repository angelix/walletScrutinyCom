---
wsId: pierWallet
title: pier wallet
altTitle: 
authors:
- danny
appId: one.nobank.app
appCountry: lb
idd: '1613187762'
released: 2022-05-11
updated: 2023-10-24
version: '3.17'
stars: 0
reviews: 0
size: '72800256'
website: https://www.pierwallet.com
repository: 
issue: 
icon: one.nobank.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-09
signer: 
reviewArchive: 
twitter: pier_wallet
social:
- https://www.linkedin.com/company/pierwallet
- https://discord.com/invite/gHeHD4fAqK
- https://www.instagram.com/pierwallet
features: 
developerName: NBK Labs AG

---

{% include copyFromAndroid.html %}