---
wsId: coinCloudWallet
title: 'Coin Cloud: Wallet'
altTitle: 
authors:
- danny
appId: com.coincloud.walletpreview
appCountry: us
idd: '1589867683'
released: 2022-02-25
updated: 2022-08-25
version: 12.9.27
stars: 2.2
reviews: 50
size: '87969792'
website: https://www.coin.cloud/
repository: 
issue: 
icon: com.coincloud.walletpreview.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2023-08-29
signer: 
reviewArchive: 
twitter: bitstopofficial
social:
- https://www.facebook.com/Bitstopofficial
- https://www.instagram.com/bitstopofficial
- https://www.youtube.com/channel/UCInWJCpASNIgENyo9uep0lA/videos
features: 
developerName: Coin Cloud

---

{% include copyFromAndroid.html %}