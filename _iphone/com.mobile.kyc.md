---
wsId: kyccWallet
title: KYCC Wallet
altTitle: 
authors:
- danny
appId: com.mobile.kyc
appCountry: us
idd: '1606034760'
released: 2022-02-01
updated: 2022-11-02
version: 1.1.0
stars: 0
reviews: 0
size: '42335232'
website: 
repository: 
issue: 
icon: com.mobile.kyc.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2023-10-30
signer: 
reviewArchive: 
twitter: kyc_coin
social:
- https://www.linkedin.com/company/kyccoin
- https://www.facebook.com/kyccoin
- https://www.instagram.com/kyc_coin
features: 
developerName: KYC&AML Ltd

---

{% include copyFromAndroid.html %}
