---
wsId: koinParkCrypto
title: KoinPark
altTitle: 
authors:
- danny
appId: com.app.KoinParkios
appCountry: in
idd: '6448510693'
released: 2023-05-10
updated: 2023-11-06
version: '2.0'
stars: 4.9
reviews: 18
size: '66358272'
website: 
repository: 
issue: 
icon: com.app.KoinParkios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-29
signer: 
reviewArchive: 
twitter: KoinparkInfo
social:
- https://www.linkedin.com/company/koinpark
- https://www.facebook.com/koinpark
- https://www.instagram.com/koinparkinfo
- https://medium.com/@koinpark
- https://t.me/samcrypto9
- https://www.youtube.com/@koinparkInfo
- https://www.reddit.com/user/Koinpark
features: 
developerName: ASSETFINX TECHNOLOGIES PRIVATE LIMITED

---

{% include copyFromAndroid.html %}