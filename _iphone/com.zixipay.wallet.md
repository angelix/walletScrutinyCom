---
wsId: ZixiPay
title: 'ZixiPay: Tether Wallet'
altTitle: 
authors:
- danny
appId: com.zixipay.wallet
appCountry: us
idd: 1492139262
released: 2019-12-22
updated: 2023-09-28
version: '1.85'
stars: 4.2
reviews: 5
size: '46364672'
website: https://zixipay.com/
repository: 
issue: 
icon: com.zixipay.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-17
signer: 
reviewArchive: 
twitter: zixipay
social:
- https://www.facebook.com/ZixiPay
features: 
developerName: ZixiPay LLC

---

{% include copyFromAndroid.html %}
