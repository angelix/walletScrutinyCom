---
wsId: ChaingeFinance
title: Chainge Finance
altTitle: 
authors:
- danny
appId: com.chainge.finance.app
appCountry: us
idd: 1578987516
released: 2021-08-04
updated: 2023-09-28
version: 0.6.9
stars: 4.3
reviews: 69
size: '103236608'
website: https://www.chainge.finance/
repository: 
issue: 
icon: com.chainge.finance.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive: 
twitter: FinanceChainge
social:
- https://www.linkedin.com/company/chainge-finance
- https://www.facebook.com/chainge.finance
features: 
developerName: Chainge Tech Limited

---

{% include copyFromAndroid.html %}
