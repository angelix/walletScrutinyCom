---
wsId: trendofx
title: 'Trendo: Stocks & Forex Trading'
altTitle: 
authors:
- danny
appId: ios.m3.Trendo
appCountry: in
idd: 1530580389
released: 2020-09-29
updated: 2023-10-16
version: 3.4.13
stars: 4.9
reviews: 15
size: '55992320'
website: 
repository: 
issue: 
icon: ios.m3.Trendo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-04
signer: 
reviewArchive: 
twitter: 
social:
- https://www.instagram.com/fxtrendo/
features: 
developerName: Trendo LLC

---

{% include copyFromAndroid.html %}
