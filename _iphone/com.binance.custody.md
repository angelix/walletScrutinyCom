---
wsId: ceffuCustody
title: 'Ceffu: Institutional Custody'
altTitle: 
authors:
- danny
appId: com.binance.custody
appCountry: sg
idd: '1595828184'
released: 2021-12-01
updated: 2023-11-17
version: 3.1.0
stars: 4.7
reviews: 3
size: '105720832'
website: https://www.ceffu.com
repository: 
issue: 
icon: com.binance.custody.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: CeffuGlobal
social:
- https://www.linkedin.com/company/ceffu
features: 
developerName: Block Custody

---

{% include copyFromAndroid.html %}