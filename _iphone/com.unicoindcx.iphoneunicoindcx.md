---
wsId: unicoinDCXTrader
title: UnicoinDCX
altTitle: 
authors:
- danny
appId: com.unicoindcx.iphoneunicoindcx
appCountry: us
idd: '1447599401'
released: 2018-12-29
updated: 2022-02-14
version: 2.8.5
stars: 5
reviews: 3
size: '31784960'
website: 
repository: 
issue: 
icon: com.unicoindcx.iphoneunicoindcx.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-09-06
signer: 
reviewArchive: 
twitter: Unicoin5
social:
- https://www.facebook.com/unicoindcx
- https://www.linkedin.com/company/unicoindcx
- https://www.instagram.com/unicoindcx
- https://www.youtube.com/channel/UCVa0AyvJni57jdQ9jlGSUOw
features: 
developerName: UNICOIN DCX SDN. BHD.

---

{% include copyFromAndroid.html %}