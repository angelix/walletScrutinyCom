---
wsId: vastCryptoBank
title: Vast Bank Mobile
altTitle: 
authors:
- danny
appId: crypto.vast.bank
appCountry: us
idd: '1572241586'
released: 2021-08-23
updated: 2023-08-15
version: 1.7.14
stars: 3.4
reviews: 83
size: '59568128'
website: https://www.vast.bank/
repository: 
issue: 
icon: crypto.vast.bank.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-02
signer: 
reviewArchive: 
twitter: vastbank
social:
- https://www.linkedin.com/company/vastbank
- https://www.facebook.com/vastbankNA
features: 
developerName: Vast Bank N.A.

---

{% include copyFromAndroid.html %}
