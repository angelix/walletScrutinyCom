---
wsId: croptyWallet
title: Crypto wallet – Bitcoin & USDT
altTitle: 
authors:
- danny
appId: com.cropty.app
appCountry: us
idd: '1624901793'
released: 2022-08-04
updated: 2023-10-18
version: 1.5.6
stars: 4.9
reviews: 84
size: '16635904'
website: https://cropty.io/
repository: 
issue: 
icon: com.cropty.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-30
signer: 
reviewArchive: 
twitter: cropty_app
social:
- https://www.youtube.com/@croptytv
features: 
developerName: Coinscatch

---

{% include copyFromAndroid.html %}