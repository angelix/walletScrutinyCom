---
wsId: finanzenZero
title: finanzen.net zero Aktien & ETF
altTitle: 
authors:
- danny
appId: de.gratisbroker.ios.mobileapp
appCountry: de
idd: '1556711130'
released: 2021-06-07
updated: 2023-11-20
version: 4.5.0
stars: 4.7
reviews: 7926
size: '125663232'
website: https://www.finanzen.net/zero/
repository: 
issue: 
icon: de.gratisbroker.ios.mobileapp.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-03-02
signer: 
reviewArchive: 
twitter: FinanzenNet
social:
- https://www.facebook.com/finanzen.net
- https://www.youtube.com/channel/UC0SfuDptovS05L3JjXSHjBg
features: 
developerName: finanzen.net zero GmbH

---

{% include copyFromAndroid.html %}
