---
wsId: globianceApp
title: Globiance
altTitle: 
authors:
- danny
appId: com.globiance.iosapp
appCountry: us
idd: '1584923932'
released: 2021-10-05
updated: 2023-10-03
version: '2.05'
stars: 4.2
reviews: 69
size: '58011648'
website: https://globiance.com
repository: 
issue: 
icon: com.globiance.iosapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-20
signer: 
reviewArchive: 
twitter: globiance
social:
- https://www.facebook.com/Globiance
- https://www.youtube.com/c/GLOBIANCE
- https://www.instagram.com/globiance
- https://t.me/globiancegroup
- https://www.linkedin.com/company/globiance
features: 
developerName: Globiance Holdings Limited

---

{% include copyFromAndroid.html %}