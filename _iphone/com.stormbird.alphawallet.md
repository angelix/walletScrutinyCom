---
wsId: AlphaWallet
title: AlphaWallet, Ethereum and EVM
altTitle: 
authors:
- danny
appId: com.stormbird.alphawallet
appCountry: us
idd: 1358230430
released: 2018-05-25
updated: 2023-10-17
version: '3.70'
stars: 4.7
reviews: 282
size: '103806976'
website: https://alphawallet.com/
repository: 
issue: 
icon: com.stormbird.alphawallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-10
signer: 
reviewArchive: 
twitter: Alpha_wallet
social:
- https://www.reddit.com/r/AlphaWallet
- https://github.com/alphawallet
features: 
developerName: Stormbird Pte. Ltd.

---

{% include copyFromAndroid.html %}
