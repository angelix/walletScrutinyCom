---
wsId: coinlord
title: COINLORD
altTitle: 
authors:
- danny
appId: com.cryptore.nine99
appCountry: in
idd: '1609787479'
released: 2022-02-16
updated: 2022-07-14
version: 1.1.8
stars: 3.5
reviews: 15
size: '15019008'
website: 
repository: 
issue: 
icon: com.cryptore.nine99.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-07-10
signer: 
reviewArchive: 
twitter: coinlord_trade
social:
- https://coinlord.org
- https://www.facebook.com/coinlordtradee
- https://www.linkedin.com/company/coinlord.trading
- https://t.me/coinlord_official
- https://www.instagram.com/coinlordtrade
- https://www.facebook.com/coinlordtradee
features: 
developerName: Nine 99 Technologies LLP

---

{% include copyFromAndroid.html %}
