---
wsId: bitci
title: 'Bitci TR: BTC | ETH | USDT'
altTitle: 
authors:
- danny
appId: com.bitcimobil.com
appCountry: tr
idd: '1459044769'
released: 2019-04-17
updated: 2023-11-09
version: 5.0.4
stars: 4.2
reviews: 1777
size: '147466240'
website: https://www.bitci.com.tr/
repository: 
issue: 
icon: com.bitcimobil.com.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-01-21
signer: 
reviewArchive: 
twitter: bitcicom
social:
- https://www.linkedin.com/company/bitcicom/
- https://www.facebook.com/bitcicom
- https://medium.com/@bitcicom
- https://www.youtube.com/channel/UCJ_cGIv6JJ249qKXWbhOtMg
features: 
developerName: Bitci Teknoloji Anonim Sirketi

---

{% include copyFromAndroid.html %}