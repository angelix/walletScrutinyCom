---
wsId: patriciaUniverse
title: Patricia Universe
altTitle: 
authors:
- danny
appId: com.patriciauniverse.app
appCountry: us
idd: '1609772820'
released: 2023-04-19
updated: 2023-11-21
version: '1.2'
stars: 1.6
reviews: 109
size: '34293760'
website: 
repository: 
issue: 
icon: com.patriciauniverse.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-29
signer: 
reviewArchive: 
twitter: PatriciaSwitch
social:
- https://mypatricia.co
- https://www.instagram.com/welcometopatricia
- https://www.facebook.com/patricia.com.ng
- https://www.youtube.com/channel/UCxfeniM2_FGIFcXSWa4Y_sA
features: 
developerName: Patricia Technologies Limited

---

{% include copyFromAndroid.html %}