---
wsId: quantfury
title: 'Quantfury: Your Global Broker'
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2023-09-25
version: 1.65.0
stars: 4.5
reviews: 47
size: '73965568'
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive: 
twitter: quantfury
social: 
features: 
developerName: Quantfury Ltd

---

{% include copyFromAndroid.html %}
