---
wsId: WealthsimpleTrade
title: Wealthsimple - Grow your money
altTitle: 
authors:
- danny
appId: com.wealthsimple.trade
appCountry: ca
idd: 1403491709
released: 2019-02-26
updated: 2023-11-24
version: 2.114.0
stars: 4.6
reviews: 124134
size: '192028672'
website: https://www.wealthsimple.com/en-ca/
repository: 
issue: 
icon: com.wealthsimple.trade.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive: 
twitter: Wealthsimple
social:
- https://www.facebook.com/wealthsimple
features: 
developerName: Wealthsimple

---

{% include copyFromAndroid.html %}
