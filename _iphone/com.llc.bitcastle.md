---
wsId: bitcastleTradeCrypto
title: 'bitcastle: crypto wallet'
altTitle: 
authors:
- danny
appId: com.llc.bitcastle
appCountry: gb
idd: '1616104862'
released: 2022-07-15
updated: 2023-11-15
version: 1.6.9
stars: 0
reviews: 0
size: '167454720'
website: https://bitcastle.io/
repository: 
issue: 
icon: com.llc.bitcastle.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-10
signer: 
reviewArchive: 
twitter: bit_castle
social:
- https://www.facebook.com/bitcastle.English
- https://t.me/bitcastle_official
features: 
developerName: bitcastle LLC

---

{% include copyFromAndroid.html %}
