---
wsId: CAPITALIKA
title: Capitalika
altTitle: 
authors: 
appId: com.capitalika.exchange
appCountry: ec
idd: '1570799130'
released: '2021-10-04'
updated: 2023-11-16
version: 1.1.0
stars: 4.6
reviews: 7
size: '4990976'
website: https://capitalika.com/
repository: 
issue: 
icon: com.capitalika.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-25
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Capitalika

---

{% include copyFromAndroid.html %}
