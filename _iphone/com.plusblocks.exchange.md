---
wsId: boxTradexExchange
title: BOXTradEx
altTitle: 
authors: 
appId: com.plusblocks.exchange
appCountry: ph
idd: '1566819984'
released: 2021-05-12
updated: 2023-11-17
version: 1.7.0
stars: 0
reviews: 0
size: '142574592'
website: 
repository: 
issue: 
icon: com.plusblocks.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-11
signer: 
reviewArchive: 
twitter: box_trad
social:
- https://www.youtube.com/channel/UCxxyhRSwE8EqWCwCwkSVHrA
features: 
developerName: PlusBlocks Technology Limited

---

{% include copyFromAndroid.html %}
