---
wsId: ankerPay
title: 'AnkerPay Wallet : Bitcoin'
altTitle: 
authors:
- danny
appId: com.ankerpay.ioswallet
appCountry: us
idd: 1487931971
released: 2019-11-22
updated: 2023-10-28
version: '1.29'
stars: 0
reviews: 0
size: '56802304'
website: https://ankerpay.com/mobile-wallet/
repository: 
issue: 
icon: com.ankerpay.ioswallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-08
signer: 
reviewArchive: 
twitter: AnkerPay
social:
- https://www.facebook.com/AnkerPlatform
features: 
developerName: AnkerPay

---

{% include copyFromAndroid.html %}