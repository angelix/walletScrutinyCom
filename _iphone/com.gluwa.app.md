---
wsId: gluwaWallet
title: Gluwa
altTitle: 
authors:
- danny
appId: com.gluwa.app
appCountry: us
idd: '1021292326'
released: 2015-08-18
updated: 2023-11-23
version: 8.5.0
stars: 4.3
reviews: 6
size: '71074816'
website: https://www.gluwa.com
repository: 
issue: 
icon: com.gluwa.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: gluwa
social:
- https://medium.com/@gluwa
- https://www.linkedin.com/company/gluwa
- https://www.youtube.com/channel/UCqJfH4lOL7keXBBDtxsz0TA
- https://www.instagram.com/gluwa.inc
features: 
developerName: Gluwa Inc.

---

{% include copyFromAndroid.html %}
