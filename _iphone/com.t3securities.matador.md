---
wsId: publicStocksCrypto
title: 'Public: Stocks/T-bills/Crypto'
altTitle: 
authors:
- danny
appId: com.t3securities.matador
appCountry: us
idd: '1204112719'
released: 2017-03-13
updated: 2023-11-21
version: 4.7.12
stars: 4.7
reviews: 66440
size: '228902912'
website: https://public.com
repository: 
issue: 
icon: com.t3securities.matador.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-21
signer: 
reviewArchive: 
twitter: public
social:
- https://www.instagram.com/publicapp
- https://www.facebook.com/PublicHello
- https://www.linkedin.com/company/publichello
- https://medium.com/the-public-blog
- https://www.tiktok.com/@public
features: 
developerName: Open to the Public Investing, Inc.

---

{% include copyFromAndroid.html %}