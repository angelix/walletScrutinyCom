---
wsId: cTrader
title: cTrader
altTitle: 
authors:
- danny
appId: com.spotware.ct
appCountry: my
idd: '767428811'
released: 2013-12-05
updated: 2023-09-15
version: 4.8.54452
stars: 4.8
reviews: 229
size: '208281600'
website: http://marketing.spotware.com
repository: 
issue: 
icon: com.spotware.ct.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-01
signer: 
reviewArchive: 
twitter: cTrader
social:
- https://www.linkedin.com/company/ctrader
- https://www.youtube.com/spotware
- https://t.me/cTrader_Official
features: 
developerName: Spotware

---

{% include copyFromAndroid.html %}
