---
wsId: gate.io
title: Gate.io - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.gateio.app.gateio-app
appCountry: id
idd: 1294998195
released: 2017-11-03
updated: 2023-11-21
version: 5.9.21
stars: 4
reviews: 349
size: '466811904'
website: https://gate.io
repository: 
issue: 
icon: com.gateio.app.gateio-app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive: 
twitter: gate_io
social:
- https://www.facebook.com/gateioglobal
- https://www.reddit.com/r/GateioExchange
features: 
developerName: GATE GLOBAL UAB

---

{% include copyFromAndroid.html %}
