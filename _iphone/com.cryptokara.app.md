---
wsId: cryptoKara
title: CryptoKara
altTitle: 
authors:
- danny
appId: com.cryptokara.app
appCountry: us
idd: '1581610129'
released: 2021-10-19
updated: 2023-10-23
version: 1.7.3
stars: 3.8
reviews: 38
size: '55916544'
website: https://crypto-kara-site.vercel.app/
repository: 
issue: 
icon: com.cryptokara.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: AUTOMATED CHAIN LIMITED

---

{% include copyFromAndroid.html %}
