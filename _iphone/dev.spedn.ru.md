---
wsId: Koshelek
title: Кошелек. Криптовалюты & Токены
altTitle: 
authors:
- danny
appId: dev.spedn.ru
appCountry: ru
idd: 1524167720
released: 2020-08-05
updated: 2022-09-15
version: 1.12.2
stars: 4.4
reviews: 100
size: '58861568'
website: https://koshelek.ru/
repository: 
issue: 
icon: dev.spedn.ru.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-09-10
signer: 
reviewArchive: 
twitter: koshelek_ru
social:
- https://www.facebook.com/koshelekru
features: 
developerName: Кошелёк.ру

---

{% include copyFromAndroid.html %}
