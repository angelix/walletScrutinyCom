---
wsId: shidoWalletDefi
title: Shido Wallet
altTitle: 
authors:
- danny
appId: com.shido.wallet
appCountry: us
idd: '6443624368'
released: 2023-01-06
updated: 2023-07-31
version: '2.7'
stars: 4.9
reviews: 50
size: '108346368'
website: 
repository: 
issue: 
icon: com.shido.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-17
signer: 
reviewArchive: 
twitter: ShidoGlobal
social:
- https://www.linkedin.com/in/shido-global-522554261
- https://discord.com/invite/9zvD93q5dW
- https://t.me/ShidoGlobal
- https://www.youtube.com/@ShidoGlobal
- https://www.facebook.com/ShidoGlobal
- https://www.instagram.com/shidoglobal
- https://www.reddit.com/r/ShidoInuOfficial
features: 
developerName: Shido Finance

---

{% include copyFromAndroid.html %}