---
wsId: kryllTrade
title: Kryll.io - Trade BTC & Crypto
altTitle: 
authors:
- danny
appId: io.kryll.app
appCountry: id
idd: '1445896905'
released: 2019-01-09
updated: 2023-09-11
version: 2.3.12
stars: 0
reviews: 0
size: '36996096'
website: https://kryll.io
repository: 
issue: 
icon: io.kryll.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-09-05
signer: 
reviewArchive: 
twitter: kryll_io
social: 
features: 
developerName: Cryptense SAS

---

{% include copyFromAndroid.html %}