---
wsId: ibtcex
title: iBTCex
altTitle: 
authors:
- danny
appId: com.iBTC.trade.hk
appCountry: hk
idd: '1489679343'
released: 2019-12-18
updated: 2022-05-18
version: 1.6.0
stars: 4.5
reviews: 4
size: '28845056'
website: https://ibtc.com.hk/
repository: 
issue: 
icon: com.iBTC.trade.hk.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social:
- https://www.instagram.com/ibtcex
- http://t.me/ibtc_official
- https://www.facebook.com/ibtcex
features: 
developerName: iBTC Technology

---

{% include copyFromAndroid.html %}
