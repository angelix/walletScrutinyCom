---
wsId: atomic
title: Atomic Wallet
altTitle: 
authors:
- leo
appId: atomicwallet
appCountry: 
idd: 1478257827
released: 2019-11-05
updated: 2023-07-20
version: 1.17.2
stars: 4.3
reviews: 15727
size: '146287616'
website: https://atomicwallet.io/
repository: 
issue: 
icon: atomicwallet.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2023-11-22
signer: 
reviewArchive: 
twitter: atomicwallet
social:
- https://www.facebook.com/atomicwallet
features: 
developerName: ATOMIC PROTOCOL SYSTEMS OÜ

---

**Update 2022-01-02**: This app is no more available.

{% include copyFromAndroid.html %}
