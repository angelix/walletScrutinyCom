---
wsId: xBankPocket
title: xBank -All-In-One Web3 Portal
altTitle: 
authors:
- danny
appId: com.tratao.xpocket
appCountry: us
idd: '1481024258'
released: 2019-11-08
updated: 2023-11-16
version: 4.4.0
stars: 4
reviews: 168
size: '99964928'
website: https://xbank.plus/
repository: 
issue: 
icon: com.tratao.xpocket.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-13
signer: 
reviewArchive: 
twitter: xBank_Official
social:
- https://discord.com/invite/PGBVwcaeQE
- https://www.linkedin.com/company/xbank-global
- https://medium.com/@xBankCrypto
features: 
developerName: INITIAL BLOCKCHAIN TECH PTE. LTD.

---

{% include copyFromAndroid.html %}