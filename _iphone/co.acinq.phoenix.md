---
wsId: phoenix
title: Phoenix Wallet
altTitle: 
authors:
- leo
- danny
appId: co.acinq.phoenix
appCountry: us
idd: 1544097028
released: 2021-07-13
updated: 2023-11-08
version: 2.0.7
stars: 4.3
reviews: 33
size: '42426368'
website: https://phoenix.acinq.co
repository: https://github.com/ACINQ/phoenix-kmm
issue: https://github.com/ACINQ/phoenix/issues/112
icon: co.acinq.phoenix.jpg
bugbounty: 
meta: ok
verdict: ftbfs
date: 2023-11-25
signer: 
reviewArchive: 
twitter: PhoenixWallet
social: 
features:
- ln
developerName: ACINQ

---

{% include copyFromAndroid.html %}
