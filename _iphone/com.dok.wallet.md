---
wsId: DokWallet
title: 'DokWallet: Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.dok.wallet
appCountry: il
idd: 1533065700
released: 2020-10-08
updated: 2023-10-20
version: '1.60'
stars: 4.8
reviews: 16
size: '55046144'
website: https://dokwallet.com
repository: 
issue: 
icon: com.dok.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-10-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Moreover4u2 Ltd

---

{% include copyFromAndroid.html %}
