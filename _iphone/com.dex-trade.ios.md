---
wsId: DexTrade
title: Dex-Trade
altTitle: 
authors:
- danny
appId: com.dex-trade.ios
appCountry: us
idd: 1496672790
released: 2020-01-28
updated: 2023-05-11
version: 2.1.9
stars: 3
reviews: 7
size: '8921088'
website: https://dex-trade.com/
repository: 
issue: 
icon: com.dex-trade.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-12-17
signer: 
reviewArchive: 
twitter: dextrade_
social:
- https://www.linkedin.com/company/dex-trade-exchange
- https://www.facebook.com/DexTradeExchange
features: 
developerName: Dex-trade

---

{% include copyFromAndroid.html %}
