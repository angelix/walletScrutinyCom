---
wsId: zedPayExchange
title: Zed-Pay
altTitle: 
authors:
- danny
appId: com.zed-pay.app.ios
appCountry: us
idd: '6444008269'
released: 2022-11-07
updated: 2023-05-24
version: 1.3.0
stars: 5
reviews: 3
size: '38815744'
website: 
repository: 
issue: 
icon: com.zed-pay.app.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-16
signer: 
reviewArchive: 
twitter: ZedPayCo
social:
- https://www.linkedin.com/company/zedpaycompany
- https://www.facebook.com/zedpayco
- https://www.instagram.com/zedpay.co
features: 
developerName: ZEDPAY

---

{% include copyFromAndroid.html %}