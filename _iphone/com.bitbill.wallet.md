---
wsId: ownbit
title: 'Ownbit: Cold & MultiSig Wallet'
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2023-10-18
version: 4.46.0
stars: 4.3
reviews: 52
size: '121671680'
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: BITBILL PTY LTD

---

{% include copyFromAndroid.html %}
